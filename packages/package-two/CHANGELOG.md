# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@2.0.1...@ahowardtech/pkg-two@2.0.2) (2023-02-22)

**Note:** Version bump only for package @ahowardtech/pkg-two





## [2.0.1](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@2.0.0...@ahowardtech/pkg-two@2.0.1) (2023-02-22)


### Bug Fixes

* 🐛 testing new version ([1c136e8](https://gitlab.com/ahoward21/lerna-turbo-enhanced/commit/1c136e8e98abeb45e03eeba6af86e4b440a4860f))






# [2.0.0](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.2.0...@ahowardtech/pkg-two@2.0.0) (2023-02-22)

### Bug Fixes

-   🐛 Testing versioning ([4d2965b](https://gitlab.com/ahoward21/lerna-turbo-enhanced/commit/4d2965b4459bcf949cc6cd54bc26156490b24ff0))

### BREAKING CHANGES

-   🧨 yes

# [0.2.0](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.1.2...@ahowardtech/pkg-two@0.2.0) (2023-02-22)

### Features

-   🎸 test update breaking change ([bd8f279](https://gitlab.com/ahoward21/lerna-turbo-enhanced/commit/bd8f27995a44cd6127749401d6982773144fa4b0))

### BREAKING CHANGES

-   🧨 yes

## [0.1.2](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.1.1...@ahowardtech/pkg-two@0.1.2) (2023-02-21)

**Note:** Version bump only for package @ahowardtech/pkg-two

## [0.1.1](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.1.0...@ahowardtech/pkg-two@0.1.1) (2023-02-21)

**Note:** Version bump only for package @ahowardtech/pkg-two

# [0.1.0](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.0.4...@ahowardtech/pkg-two@0.1.0) (2023-02-21)

### Features

-   🎸 major feature test ([57dff27](https://gitlab.com/ahoward21/lerna-turbo-enhanced/commit/57dff27f81cdad958c56ca8ed4ca97551f1b1f65))

### BREAKING CHANGES

-   🧨 major feature

## [0.0.4](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.0.3...@ahowardtech/pkg-two@0.0.4) (2023-02-21)

**Note:** Version bump only for package @ahowardtech/pkg-two

## [0.0.3](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.0.2...@ahowardtech/pkg-two@0.0.3) (2023-02-21)

**Note:** Version bump only for package @ahowardtech/pkg-two

## [0.0.2](https://gitlab.com/ahoward21/lerna-turbo-enhanced/compare/@ahowardtech/pkg-two@0.0.1...@ahowardtech/pkg-two@0.0.2) (2023-02-21)

**Note:** Version bump only for package @ahowardtech/pkg-two

## 0.0.1 (2023-02-03)

**Note:** Version bump only for package @ahowardtech/pkg-two
